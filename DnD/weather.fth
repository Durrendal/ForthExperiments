# Explanation:
Rulset: Dnd 5e
On a D100 role the following weights are held:
  D%   |  Weather  |    Cold Climate     |  Temperate Climate      |  Desert    |
-------+-----------+---------------------+-------------------------+------------+
01-70  |  Normal   |      Cold/Calm      |    Seasonal Norm        |  Hot/Calm  |
71-80  |  Abnormal | Heat Wave/Cold Snap |   Heat Wave/Cold Snap   |  Hot/Windy |
81-90  | Inclement |      Rain/Snow      | Rain (norm for Season)  |  Hot/Windy |
91-99  |   Storm   |     Snow Storm      |  Thunder/Snow Storm     |  Duststorm |
100    |    Gale   |      Blizzard       |  Hurricane, Tornado,    |  Downpour  |
       |           |                     |  Blizzard, Windstorm    |            |

~~~
:n:ranged-random (lower,upper--n) over - n:inc n:random swap mod + ;

:ranged-case
  [ 'abc 'aabc reorder n:between? ] dip swap
  [ nip call TRUE ] [ drop FALSE ] choose 0; pop drop drop ;

:d4 #1 #4 n:ranged-random ;
:d6 #1 #6 n:ranged-random ;
:d8 #1 #8 n:ranged-random ;
:d10 #1 #10 n:ranged-random ;
:d12 #1 #12 n:ranged-random ;
:d20 #1 #20 n:ranged-random ;
:d100 #1 #100 n:ranged-random ;

:weather (n--s)
#1 #70 [ 'Normal s:put nl ] ranged-case
#71 #80 [ 'Abnormal s:put nl ] ranged-case
#81 #90 [ 'Inclement s:put nl ] ranged-case
#91 #99 [ 'Storm s:put nl ] ranged-case
#100 eq? [ 'Gale s:put nl ] if ;

d100 weather
~~~
