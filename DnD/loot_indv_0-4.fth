# Explanation:
Rulset: Dnd 5e


~~~
:n:rrand (lower,upper--n) over - n:inc n:random swap mod + ;
:ranged-case
  [ 'abc 'aabc reorder n:between? ] dip swap
  [ nip call TRUE ] [ drop FALSE ] choose 0; pop drop drop ;

:roll (dice,dotimes--n)
  'd6 [ #1 #6 n:rrand ] s:case
  'd8 [ #1 #8 n:rrand ] s:case
  'd10 [ #1 #10 n:rrand ] s:case
  'd12 [ #1 #12 n:rrand ] s:case
  'd20 [ #1 #20 n:rrand ] s:case
  'd30 [ #1 #30 n:rrand ] s:case
  'd100 [ #1 #100 n:rrand ] s:case ;

'result var

:indv_0-4 (n--n)
#1 #30 [ #5 [ 'd6 roll @result + !result ] times #17 @result + ] ranged-case
#31 #60 [ #4 [ 'd6 roll @result + !result ] times #14 @result + ] ranged-case
#61 #70 [ #3 [ 'd6 roll @result + !result ] times #10 @result + ] ranged-case
#71 #95 [ #3 [ 'd6 roll @result + !result ] times #10 @result + ] ranged-case
#96 #100 [ #1 [ 'd6 roll @result + !result ] times #3 @result + ] ranged-case ;

#0 !result
'd100 roll indv_0-4 n:put nl
~~~
