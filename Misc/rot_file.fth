~~~
'nopqrstuvwxyzabcdefghijklm 'MAP s:const
:encode (c-c) $a - MAP + fetch ;
:rot13 (s-s) s:to-lower [ dup c:letter? [ encode ] if ] s:map ;

#0 script:get-argument [ rot13 s:put nl ] file:for-each-line
~~~
