Upon decrementing the counter to zero, interrupt the program.
Inspired by William Gibson's Count Zero

Setup variables, create a ranged random number function
~~~
'interrupt var

:n:rrand (l,u--n) over - n:inc n:random swap mod + ;
~~~

If: interrupt = 0 return True/False
If: interrupt != 0 return True/False
Increment counter randomly

~~~
:interrupt #0 eq? ;
:-interrupt #0 -eq? ;
:count #0 #999 n:rrand !interrupt ;
~~~

Populate variable initially
While Interrupt var is not true
Check if interrupt is still not true
If false, recount
If true, Interrupt!

~~~
count
@interrupt -interrupt [ @interrupt -interrupt [ 'TRUE pop drop @interrupt n:put count ] [ 'FALSE nl 'COUNT_ZERO_INTERRUPT s:put nl bye ] choose ] while
~~~
